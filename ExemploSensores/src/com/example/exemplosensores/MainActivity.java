package com.example.exemplosensores;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.widget.MediaController;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {
	private SensorManager mSensorManager;
	private Sensor mSensor[];
	private TextView azimuthText;
	private TextView textview2;
	private TextView textview3;
	private TextView textview4;
	private TextView textview5;
	private TextView textview6;
	private TextView textview7;
	private TextView textview8;
	private TextView textview9;
	private TextView textview10;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor = new Sensor[20];
		mSensor[0] = mSensorManager
				.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		mSensor[1] = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensor[2] = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
		mSensor[3] = mSensorManager
				.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
		mSensor[4] = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		mSensor[5] = mSensorManager
				.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
		mSensor[6] = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
		mSensor[7] = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		mSensor[7] = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		mSensor[8] = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

		for (Sensor s : mSensor) {
			if (s != null) {
				mSensorManager.registerListener(this, s,
						SensorManager.SENSOR_DELAY_GAME);
			}
		}

		azimuthText = (TextView) findViewById(R.id.textView1);
		textview2 = (TextView) findViewById(R.id.textView2);
		textview3 = (TextView) findViewById(R.id.textView3);
		textview4 = (TextView) findViewById(R.id.textView4);
		textview5 = (TextView) findViewById(R.id.textView5);
		textview6 = (TextView) findViewById(R.id.textView6);
		textview7 = (TextView) findViewById(R.id.textView7);
		textview8 = (TextView) findViewById(R.id.textView8);
		textview9 = (TextView) findViewById(R.id.textView9);
		textview10 = (TextView) findViewById(R.id.textView10);
		
	}

	@Override
	protected void onPause() {

		mSensorManager.unregisterListener(this);
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	float[] aValues;
	float[] mValues;

	@Override
	public void onSensorChanged(SensorEvent event) {
		
		switch (event.sensor.getType()) {
		
		case Sensor.TYPE_ACCELEROMETER:
			aValues = event.values.clone();
			textview7.setText("Accelerometer:" + event.values[0] + ","
					+ event.values[1] + "," + event.values[2]);
			break;
		case Sensor.TYPE_MAGNETIC_FIELD:
			mValues = event.values.clone();
			break;
		case Sensor.TYPE_PRESSURE:
			textview2.setText("Pressure:" + event.values[0]);
			break;
		case Sensor.TYPE_AMBIENT_TEMPERATURE:
			textview3.setText("Temperature:" + event.values[0]);
			break;
		case Sensor.TYPE_LIGHT:
			textview4.setText("Light:" + event.values[0]);
			break;
		case Sensor.TYPE_RELATIVE_HUMIDITY:
			textview5.setText("Humidity:" + event.values[0]);
			break;
		case Sensor.TYPE_PROXIMITY:
			textview6.setText("Proximity:" + event.values[0]);
			break;
		case Sensor.TYPE_GRAVITY:
			textview8.setText("Gravity:" + event.values[0] + ","
					+ event.values[1] + "," + event.values[2]);
			break;
		case Sensor.TYPE_GYROSCOPE:
			textview9.setText("GYROSCOPE:" + event.values[0] + ","
					+ event.values[1] + "," + event.values[2]);
			break;
		case Sensor.TYPE_ROTATION_VECTOR:
			textview10.setText("ROTATION_VECTOR:" + event.values[0] + ","
					+ event.values[1] + "," + event.values[2]+","+(event.values.length>3?event.values[3]:"No Implemented"));
			break;
			
		}

		float[] R = new float[16];
		float[] remappedR = new float[16];
		float[] orientationValues = new float[3];
		if (aValues != null && mValues != null) {
			SensorManager.getRotationMatrix(R, null, aValues, mValues);
			SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_X,
					SensorManager.AXIS_Z, remappedR);
			SensorManager.getOrientation(remappedR, orientationValues);
			float newAzimuth = (float) Math.round(Math
					.toDegrees(orientationValues[0]));
			azimuthText.setText("azimuth: " + newAzimuth);
		}
	}

}
