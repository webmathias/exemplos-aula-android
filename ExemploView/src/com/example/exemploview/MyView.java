package com.example.exemploview;

import java.io.IOException;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class MyView extends View {
	public MyView(Context context) {
		super(context);

	}

	public MyView(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
//		AssetManager assetManager = getContext().getAssets();
//		try {
//			Bitmap img = BitmapFactory.decodeStream(assetManager
//					.open("iconeandroid.png"));
//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}
		Bitmap img = BitmapFactory.decodeResource(getResources(),
				R.drawable.iconeandroid);
		Paint p = new Paint();
		p.setAntiAlias(true);
		p.setFilterBitmap(true);
		p.setStyle(Style.STROKE);
		p.setColor(Color.BLUE);
		canvas.drawRect(50, 50, 100, 100, p);
		canvas.save();
		{
			p.setColor(Color.RED);
			canvas.scale(0.5f, 0.5f);
			canvas.rotate(15, 100, 100);
			canvas.drawRect(75, 75, 125, 125, p);
		}
		canvas.restore();

		// canvas.save();
		// {
		// canvas.scale(sx, sy);
		// canvas.rotate(angulo, x, y);
		// Path path = new Path();
		// path.moveTo(x, y);
		// path.lineTo(x, y);
		// canvas.clipPath(path, Op.REPLACE);
		// }
		// canvas.restore();

		// canvas.drawPoint(arg0, arg1, arg2);
		// canvas.drawLine(x1, y1,x2,y2,p);
		// canvas.drawRect(x1,y1,x2,y2,p);
		// canvas.drawCircle(x,y,raio,p);
		// canvas.drawArc(new RectF(x1,y1,x2,y2), anguloInicio, anguloAbertura,
		// usarCentro, p);
		// canvas.drawColor(Color.BLUE);
		// canvas.drawText(text, x, y, p);
		// canvas.drawBitmap(bitmap, x, y, p);
		// canvas.drawBitmap(bitmap, src, dst, p);

		super.onDraw(canvas);
	}

}
