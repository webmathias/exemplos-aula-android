package com.example.exemploaccountmanager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		AccountManager am = AccountManager.get(this); // "this" references the current Context

		Account[] accounts = am.getAccounts();//AccountsByType("com.google");
		TextView t = (TextView)findViewById(R.id.textView1);
		String a = "";
		for (Account account : accounts) {
			a += account.name+" - "+account.type+"\n";
		}
		t.setText(a);
		 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
