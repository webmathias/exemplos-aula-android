package com.example.exemploservice;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
	ServiceConnection con;
	public void conectarService(View v){
		if(con == null){
			con = new ServiceConnection() {
				
				@Override
				public void onServiceDisconnected(ComponentName arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onServiceConnected(ComponentName arg0, IBinder arg1) {
					 Servico1 s = ((Servico1.Servico1Binder)arg1).getServico();
					 Toast.makeText(MainActivity.this, "Contagem do Serviço"+s.cont, Toast.LENGTH_SHORT).show();
					 
					
				}
			};
			Intent in = new Intent(this, Servico1.class);			
			bindService(in, con, Service.BIND_AUTO_CREATE);
			

		}else{
			unbindService(con);
			con = null;
		}
	}
	public void startService(View v) {
		Intent in = new Intent(this, Servico1.class);
		startService(in);

		// Inicia Serviço2
		Intent intent = new Intent(this, Service2.class);
		Messenger messenger = new Messenger(handlerRetornoServido2);
		intent.putExtra("MESSENGER", messenger);		 
		intent.putExtra("param1", "parametro para o serviço");
		startService(intent);
	}

	public void stopService(View v) {
		 
		Intent in = new Intent(this, Servico1.class);
		stopService(in);
		Toast.makeText(this, "parando", Toast.LENGTH_SHORT).show();
		
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	private Handler handlerRetornoServido2 = new Handler() {
		public void handleMessage(Message message) {
			Object objRetorno = message.obj;
			if (message.arg1 == RESULT_OK && objRetorno != null) {
				Toast.makeText(MainActivity.this,
						"Retorno OK" + objRetorno.toString(), Toast.LENGTH_LONG)
						.show();
			} else {
				Toast.makeText(MainActivity.this, "Recebido código de erro do serviço.",
						Toast.LENGTH_LONG).show();
			}

		};
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
