package com.example.exemplointentcomretorno;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class Ativity2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ativity2);
		TextView txt = (TextView )findViewById(R.id.textView1);
		int a = getIntent().getIntExtra("a", 0);
		int b = getIntent().getIntExtra("b", 0);
		txt.setText(a+"+"+b);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_ativity2, menu);
		return true;
	}

	public void calcula(View v) {
		int a = getIntent().getIntExtra("a", 0);
		int b = getIntent().getIntExtra("b", 0);
		Intent data = new Intent();
		data.putExtra("resultado", a+b);
		setResult(RESULT_OK, data);
		finish();
	}
}
