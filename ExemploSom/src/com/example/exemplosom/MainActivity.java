package com.example.exemplosom;

import junit.framework.Test;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void simplesplay(View v) {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.s1);   
        mp.start();
	}
	public void psound1(View v){
		Sons.playSound(getApplicationContext(), Sons.S1);
	}
	public void psound2(View v){
		Sons.playSound(getApplicationContext(), Sons.S2);
	}
	public void psound3(View v){
		Sons.playSound(getApplicationContext(), Sons.S3);
	}

}
