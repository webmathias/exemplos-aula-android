package com.example.exemplosom;

import java.util.HashMap;

import android.R.integer;
import android.content.ContentProvider;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class Sons {
	public static final int S1 = R.raw.s1;
	public static final int S2 = R.raw.s2;
	public static final int S3 = R.raw.s3;
	public static SoundPool soundPool;
	public static HashMap<Integer, Integer> soundPoolMap;
	public static void initSounds(Context context) {
		soundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 100);
		soundPoolMap = new HashMap<Integer, Integer>(3);
		soundPoolMap.put(S1, soundPool.load(context, R.raw.s1, 1));
		soundPoolMap.put(S2, soundPool.load(context, R.raw.s2, 2));
		soundPoolMap.put(S3, soundPool.load(context, R.raw.s3, 3));
	}
	public static void playSound(Context context, int soundID) {
		if (soundPool == null || soundPoolMap == null) {
			initSounds(context);
		}
		
		float volume = 1;
		soundPool.play(soundPoolMap.get(soundID), volume, volume, 1, 0, 1f);
		
	}
}
