package com.example.exemplosocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	class enviaObjeto extends AsyncTask<Void, Integer, Object> {
		@Override
		protected Object doInBackground(Void... params) {
			try {
				Socket conn = new Socket("10.0.2.2", 8080);

				ObjectInputStream oout = new ObjectInputStream(
						conn.getInputStream());
				 
				try {
					Object recebido = oout.readObject();
					return recebido;
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					// oout.flush();
					oout.close();
					conn.close();
				}

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Object result) {
			if (result != null && result instanceof Fase) {
				Fase faseatual = (Fase) result;
				Toast.makeText(
						getApplicationContext(),
						"Recebido:" + faseatual.x1_quadrado + " , " + ""
								+ faseatual.y1_quadrado + " , "
								+ faseatual.x2_quadrado + " , "
								+ faseatual.y2_quadrado + "  ",
						Toast.LENGTH_LONG).show();

			}
			super.onPostExecute(result);
		}

	}

	public void click(View v) {
		enviaObjeto enviador = new enviaObjeto();

		enviador.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
