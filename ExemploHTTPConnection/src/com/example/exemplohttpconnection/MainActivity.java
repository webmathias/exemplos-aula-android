package com.example.exemplohttpconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private class DownloadWebpageText extends
			AsyncTask<String, Integer, String> {
		@Override
		protected void onPostExecute(String result) {
			 
			((TextView)findViewById(R.id.textView1)).setText(result);
		}
		String erro = "";
		@Override
		protected void onCancelled() {
			((TextView)findViewById(R.id.textView1)).setText("Erro ao encontrar urls:"+erro);
			
			super.onCancelled();
		}
		@Override
		protected void onProgressUpdate(Integer... values) {
			((TextView)findViewById(R.id.textView1)).setText("Progress:"+values[0]);
			super.onProgressUpdate(values);
		}
		@Override
		protected String doInBackground(String... params) {
			StringBuilder html = new StringBuilder();
			int cont=0;
			for (String string : params) {
				 InputStream is = null;
				    publishProgress(cont++);
				    try {
				        URL url = new URL(string);
				        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				        conn.setReadTimeout(10000 /* milliseconds */);
				        conn.setConnectTimeout(15000 /* milliseconds */);
				        conn.setRequestMethod("GET");
				        conn.setDoInput(true);
				       
				        conn.connect();
				        int response = conn.getResponseCode();
				       
				        is = conn.getInputStream();

				        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
				        String linha;
				        while((linha = rd.readLine()) != null){
				        	html.append(linha);
				        }
				       
				        
				    // Makes sure that the InputStream is closed after the app is
				    // finished using it.
				    }catch(Exception e){
				    	e.printStackTrace();
				    	erro = e.getMessage();
				    			cancel(true);
				    } finally {
				        if (is != null) {
				            try {
								is.close();
							} catch (IOException e) {
								 
							}
				        } 
				    }
			}
			Log.d("HTML", html.toString());
			return html.toString();
		}

	}

	public void click(View v) {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			new DownloadWebpageText().execute("http://www.globo.com/");
		} else {
			Toast.makeText(getApplicationContext(), "No Internet", Toast.LENGTH_LONG).show();
		}
	}
}
