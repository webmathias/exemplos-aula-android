package com.mathias.exemplolistapersonalizada;

import java.io.File;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	class item{
		String nome;
		boolean selecionado;
		public item(String nome, boolean selecionado) {
			super();
			this.nome = nome;
			this.selecionado = selecionado;
		}
		
	}
		class AdapterPersonalizado extends BaseAdapter{
			private Context context; 
			  public AdapterPersonalizado(Context context) {			 
				  
				 this.context = context;
			 
			}
	
			@Override
			  public View getView(int position, View convertView, ViewGroup parent) {
			    LayoutInflater inflater = (LayoutInflater) context
			        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    View rowView = convertView;
			    if(rowView == null)
			    	rowView = inflater.inflate(R.layout.row, parent, false);
			    TextView textView = (TextView) rowView.findViewById(R.id.textView1);
			    ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView1);
			    textView.setText(itens[position].nome);
			    
			    if (itens[position].selecionado) {
			      imageView.setImageResource(android.R.drawable.btn_star_big_on);
			    } else {
			      imageView.setImageResource(android.R.drawable.btn_star_big_off);
			    }
	
			    return rowView;
			  }
	
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return itens.length;
			}
	
			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return itens[position];
			}
	
			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return itens[position].hashCode();
			}
		}
	ListView lista;
	item[] itens;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		lista = (ListView)findViewById(R.id.listView1);
		overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
		itens = new item[]{
				new item("Teste1", true),	
				new item("Teste2", true),	
				new item("Teste3", false),	
				new item("Teste4", true),	
				new item("Teste5", false),	
				new item("Teste6", false),	
				new item("Teste7", false),	
				new item("Teste8", true),	
				new item("Teste9", true),	
				new item("Teste10", true),	
		};
		 
		lista.setAdapter(new AdapterPersonalizado(getApplicationContext()));
		lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView arg0, View arg1, int arg2,
					long arg3) {
					Toast.makeText(getApplicationContext(), "Clicado:"+arg2 + " - "+itens[arg2].selecionado, Toast.LENGTH_SHORT).show();
					itens[arg2].selecionado = !itens[arg2].selecionado;
					
					//lista.invalidateViews();
					((AdapterPersonalizado)arg0.getAdapter()).notifyDataSetChanged();
			}
		});
	}
public void ativandoMenu(MenuItem item){
	Toast.makeText(getApplicationContext(), "Menu Clicado", Toast.LENGTH_LONG).show();
//	URL a = new URL("http://google.com.br");
//	URLConnection con = a.openConnection();
//	con.getin'
	file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/img1.jpg");
	Intent a = new Intent(getApplicationContext(),MainActivity.class);
	a.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
	startActivityForResult(a,99);
//	startService(service)
	 
}
File file;
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	switch (requestCode) {
	case 99:
		if(resultCode == RESULT_OK){
			
		}
		break;

	default:
		break;
	}
	super.onActivityResult(requestCode, resultCode, data);
}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
