package com.example.exemplobancodedados2;

import android.os.AsyncTask;
import android.os.Bundle; 
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase; 
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	class selectBanco extends AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... params) {
			String retorno = "";

			MyDB mydb = new MyDB(getApplicationContext());
			SQLiteDatabase db = mydb.getReadableDatabase();
			Cursor c = db.rawQuery("select nome from Produtos",
					null);
			if (c != null) {
				c.moveToFirst();
				while (c.moveToNext()) {
					retorno += c.getString(0) + "\n";
				}
			}
			return retorno;
		}

		@Override
		protected void onPostExecute(String result) {
			((TextView) findViewById(R.id.texto)).setText(result);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		new selectBanco().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
