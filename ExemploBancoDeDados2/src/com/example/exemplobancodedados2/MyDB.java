package com.example.exemplobancodedados2;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDB extends SQLiteOpenHelper {

	Context context;

	public MyDB(Context c) {
		super(c, "MyDB", null, 4);
		context = c;
	}
	private String readFile() throws IOException{
		String retorno = "";
		InputStream in = context.getAssets().open("exemplo1.sql");
		int lido=0;
		byte[] buff = new byte[1024];
		while((lido = in.read(buff))>0){
			retorno += new String(buff,0,lido);
		}
		
		in.close();
		
		return retorno;
	}
	@Override
	public void onCreate(SQLiteDatabase db) {

		try {
			String sql = readFile();
			
			String sqls[] = sql.split(";");
			for(String sql1 : sqls){
				if(!sql1.trim().equals("")){
				Log.d("Database - Create", sql1);
				db.execSQL(sql1);
				}
			}
		} catch (SQLException e) { 
			e.printStackTrace();
		} catch (IOException e) { 
			e.printStackTrace();
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 onCreate(db);

	}

}
