package com.example.exemplolistviewinform;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	List<String> valores = new LinkedList<String>();
	BaseAdapter adapter;
	ListView list ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		valores.add("Item1");
		valores.add("Item1");
		valores.add("Item1");
		valores.add("Item1");
		 list = (ListView)findViewById(R.id.listView1);
		adapter = new BaseAdapter() {
			
			@Override
			public View getView(int i, View v, ViewGroup arg2) {
				 View row = v;
				 if(row == null){
					 LayoutInflater inf = MainActivity.this.getLayoutInflater();
					 row = inf.inflate(R.layout.row, null);
					 TextView txt = (TextView)row.findViewById(R.id.textView1);
					 txt.setText(valores.get(i));
					 
				 }
				return row;
			}
			
			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return arg0;
			}
			
			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return valores.get(arg0);
			}
			
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return valores.size();
			}
		};
		list.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	Random r  = new Random();
	public void addItem(View v){
		valores.add("Item:"+r.nextInt());
		
		adapter.notifyDataSetChanged();
		list.invalidate();
	}

}
