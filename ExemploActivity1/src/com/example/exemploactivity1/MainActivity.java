package com.example.exemploactivity1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Picture;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {
	public void botao(View v){
		Intent i = new Intent(
				getApplicationContext(),
				Activit2.class);
		i.putExtra("a", "teste");
//		startActivity(i);
		startActivityForResult(i, 88);
	
	}
	@Override
	protected void onActivityResult(int requestCode,
			int resultCode, Intent data) {
		// TODO Auto-generated method stub
		String retorno = data.getStringExtra(
				"retorno");
		TextView txt = (TextView)findViewById(
				R.id.textView1);
		txt.setText(retorno);
		super.onActivityResult(requestCode, resultCode, data);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	@Override
	protected void onResume() {
		Spinner sp = (Spinner)findViewById(R.id.spinner1);
		String[] vetor = new String[]{"item1", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item", "segundo item"};
		sp.setAdapter(new ArrayAdapter<String>(getApplicationContext(), 
				android.R.layout.simple_spinner_item,
				vetor));
		super.onResume();
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
