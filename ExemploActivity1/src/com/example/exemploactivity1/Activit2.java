package com.example.exemploactivity1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Activit2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activit2);
	}
	@Override
	protected void onResume() {
		 String a = getIntent().getStringExtra("a");
		 TextView txt = (TextView)findViewById(
				 R.id.textView1);
		 
		 txt.setText(a);
		super.onResume();
	}

	public void botao2(View v) {
		// Intent i = new Intent(getApplicationContext(),MainActivity.class);
		// startActivity(i);
		Intent i = new Intent();
		i.putExtra("retorno", "valor do retorno");
		setResult(89, i);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_activit2, menu);
		return true;
	}

}
