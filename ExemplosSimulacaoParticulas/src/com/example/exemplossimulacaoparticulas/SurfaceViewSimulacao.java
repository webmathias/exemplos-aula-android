package com.example.exemplossimulacaoparticulas;

import java.util.LinkedList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class SurfaceViewSimulacao extends SurfaceView implements SurfaceHolder.Callback {

	public SurfaceViewSimulacao(Context context, AttributeSet attrs) {
		super(context, attrs);
		getHolder().addCallback(this);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		synchronized (particulas) {
			for (int i = 0; i < event.getPointerCount(); i++) {

				// particulas.add(new Particula(//
				// event.getX(i),//
				// event.getY(i), //
				// (float) (Math.random() * 360), //
				// (float) (0.1), //
				// (float) (Math.random() * 10000 + 1000)));
				Particula p = new Particula(//
						event.getX(i),//
						event.getY(i), //
						(float) (Math.random() * 360), //
						(float) (Math.random() *0.5), //
						(float) (Math.random() * 10000 + 1000));
				particulas.add(p);
				if (Math.random() > 0.2d) {
					Particula lider = particulas.get((int) (Math.random() * particulas.size() - 1));
					if (lider != p) {
						p.lider = lider;
					}
				}
			}

		}

		return true;
	}

	boolean vivo;
	Thread t;
	int x = 0;
	long diftime;
	LinkedList<Particula> particulas = new LinkedList<Particula>();

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		for (int i = 0; i < 20; i++) {
			Particula p = new Particula(//
					(float) (Math.random() * getWidth()),//
					(float) (Math.random() * getHeight()), //
					(float) (Math.random() * 360), //
					(float) (Math.random() *0.5), //
					(float) (Math.random() * 10000 + 1000));
			particulas.add(p);
			if (Math.random() > 0.2d) {
				Particula lider = particulas.get((int) (Math.random() * particulas.size() - 1));
				if (lider != p) {
					p.lider = lider;
				}
			}
		}
		t = new Thread() {
			public void run() {
				LinkedList<Particula> mortos = new LinkedList<Particula>();

				vivo = true;
				Paint p = new Paint();
				SurfaceHolder h = getHolder();
				while (vivo) {

					long time = System.currentTimeMillis();
					Canvas c = h.lockCanvas();
					if (c != null) {
						// c.drawArc(oval, startAngle, sweepAngle, useCenter,
						// paint)
						synchronized (particulas) {
							c.drawColor(Color.WHITE);

							for (Particula part : particulas) {
								part.draw(c, p);
							}
							p.setColor(Color.BLACK);
							c.drawText("Diftime:" + diftime, 50, 50, p);
							h.unlockCanvasAndPost(c);

							mortos.clear();
							for (Particula part : particulas) {

								part.update(diftime);
								if (part.vida < 0) {
									mortos.add(part);
									continue;
								}
//								if (part.x < 0 || part.x > getWidth()) {
//									part.x = (part.x + getWidth()) % getWidth();
//								}
//								if (part.y < 0 || part.y > getHeight()) {
//									part.y = (part.y + getHeight()) % getHeight();
//								}
								int cor = Color.BLACK;
								for (Particula part2 : particulas) {
									if (part.colide(part2)) {

									}
								}

							}
							particulas.removeAll(mortos);
							long time1 = System.currentTimeMillis();
							diftime = time1 - time;
						}

					}

				}

			};
		};
		t.start();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		vivo = false;
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
