package com.example.exemplossimulacaoparticulas;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Particula {

	public Particula(float x, float y, float direcao, float velocidade, float vida) {
		super();
		this.x = x;
		this.y = y;
		this.direcao = direcao;
		this.velocidade = velocidade;
		velocidadeInicial = velocidade;
		this.vida = vida;
	}

	float x;
	float y;
	float direcao;
	float velocidade;
	float vida;
	float velocidadeInicial;
	Particula lider;

	public void draw(Canvas c, Paint p) {
		p.setColor(cor);
		if (tempoProximaCurva > 0)
			p.setAlpha(128);
		else
			p.setAlpha(255);
		if (lider != null) {
			p.setColor(Color.YELLOW);
		}

		float size = Math.min(20, vida / 20);
		c.drawCircle(x, y, size, p);
		p.setColor(Color.RED);
		c.drawText(contcurva+"", x, y, p);
		if (lider != null) {
			p.setColor(Color.BLACK);
			p.setAlpha(128);
			c.drawLine(x, y, lider.x, lider.y, p);
		}
		p.setAlpha(255);
	}

	float contcurva = -2;
	float curva = 45;
	boolean direita=true;
	public void update(long time) {
		x += Math.cos(Math.toRadians(direcao)) * (velocidade * (time));
		y += Math.sin(Math.toRadians(direcao)) * (velocidade * (time));
		vida -= time;
		tempoProximaCurva -= time;
		if(direita){
			contcurva += 0.05;
			if(contcurva > 1){
				direita = false;
			}
		}else{
			contcurva -= 0.05;
			if(contcurva < -1){
				direita = true;
			}
		}
		 

		if (lider != null) {

			float distanciaLider = (float) Math.sqrt(Math.pow(x - lider.x, 2) + Math.pow(y - lider.y, 2));
			 
			 if(20 >= distanciaLider){
				
			 }else{
				 direcao = (float) Math.toDegrees(Math.atan2(lider.y - y, lider.x - x))+(curva*contcurva);
				 
			 }
			if (lider.vida <= 0) {
				lider = null;
			}
		}else{
//			direcao += 10 *  contcurva;
		}

		velocidade = (float) Math.min(velocidadeInicial, velocidade + 0.1);
	}

	int cor = Color.BLACK;
	long tempoProximaCurva = 100;

	public boolean colide(Particula p2) {
		if (p2 == this)
			return false;
		if (tempoProximaCurva > 0)
			return false;
		float raio = 20;

		if (raio + raio >= Math.sqrt(Math.pow(x - p2.x, 2) + Math.pow(y - p2.y, 2))) {
			// colidiu
			float maxCurva = 45;
			//curva = (float) (Math.random() * maxCurva) - (maxCurva / 2);
			tempoProximaCurva = 100;
			velocidade = (float) Math.max(velocidade - 0.1, 0.1);

			return true;
		}
		return false;
	}
}
