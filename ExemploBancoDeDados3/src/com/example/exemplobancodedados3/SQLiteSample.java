package com.example.exemplobancodedados3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SQLiteSample extends Activity {
	/** Called when the activity is first created. */
	public static String DB_PATH = "/mnt/sdcard/baseteste";

	public void lista() {
		// TODO SQL para listar todos os elementos 
		
		
		Intent it = new Intent(this, Lista.class);
		it.putExtra("col1",  new String[]{"a","b"});
		it.putExtra("col2", new String[]{"1","2"});
		startActivity(it);
	}
	
	
	
	
	public void cadastro() {
		Intent i = new Intent(this, Cadastro.class);
		startActivity(i);

	}
	public void filtro() {
		Intent i = new Intent(this, Filtro.class);
		startActivity(i);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Button btlista = (Button) findViewById(R.id.Button01);
		btlista.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { 
				lista();
			}
		});
		Button btcad = (Button) findViewById(R.id.Button02);
		btcad.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cadastro();
			}
		});
		Button btsql = (Button) findViewById(R.id.Button03);
		btsql.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { 
				filtro();
			}
		});
		
	}
}