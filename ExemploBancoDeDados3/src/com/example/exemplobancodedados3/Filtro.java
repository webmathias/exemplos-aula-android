package com.example.exemplobancodedados3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Filtro extends Activity {
	 
	public void filtrar(String campo1, String campo2, String campo3,
			String campo4, String campo5) {
	 // TODO SQL para filtro
		
		
		Intent it = new Intent(this, Lista.class);
		it.putExtra("col1",  new String[]{});
		it.putExtra("col2", new String[]{});
		startActivity(it);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filtro);
	}

	@Override
	protected void onStart() { 
		super.onStart();

		Button bt = (Button) findViewById(R.id.Button04);
		bt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText ed1 = (EditText) findViewById(R.id.EditTextcampo1);
				EditText ed2 = (EditText) findViewById(R.id.EditTextcampo2);
				EditText ed3 = (EditText) findViewById(R.id.EditTextcampo3);
				EditText ed4 = (EditText) findViewById(R.id.EditTextcampo4);
				EditText ed5 = (EditText) findViewById(R.id.EditTextcampo5);
				filtrar(ed1.getText().toString(), ed2.getText().toString(), ed3
						.getText().toString(), ed4.getText().toString(), ed5
						.getText().toString());

			}
		});
	}

}