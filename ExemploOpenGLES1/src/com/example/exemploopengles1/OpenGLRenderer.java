package com.example.exemploopengles1;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.example.exemploopengles1.tetris.Peca;

import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

public class OpenGLRenderer implements Renderer {

	private Cube mCube = new Cube();
	private float mCubeRotation;
	private Peca p = Peca.PecaL();
	private int[][] matriz = new int[8][15];
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);

		gl.glClearDepthf(1.0f);
		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glDepthFunc(GL10.GL_LEQUAL);

		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
		new Thread(){
			public void run() {
				while(true){
					if(!p.anda(matriz)){
						p = Peca.PecaL();
					}
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			};
		}.start();
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();

		
			
			for (int i = 0; i < p.peca.length; i++) {
				for (int j = 0; j < p.peca[i].length; j++) {
					gl.glPushMatrix();
					{
					if(p.peca[i][j] > 0){
					
						gl.glTranslatef(p.x+i, -(p.y+j), -40.0f);
						mCube.draw(gl);
					}
					}
					gl.glPopMatrix();
					gl.glLoadIdentity();
					
				}
			}
		
		
		mCubeRotation -= 0.15f;
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f,
				100.0f);
		gl.glViewport(0, 0, width, height);

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
	}
}




