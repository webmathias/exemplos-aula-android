package com.example.exemploarmazenamento;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.provider.UserDictionary;
import android.util.Log;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	boolean mudo = false;

	public void salvaSharedPreferences() {
		SharedPreferences settings = getSharedPreferences("NomePreferências",
				MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("mudo", mudo);
		editor.commit();
	}

	public void lerSharedPreferences() {
		SharedPreferences settings = getSharedPreferences("NomePreferências",
				MODE_PRIVATE);
		mudo = settings.getBoolean("mudo", false);
	}

	public void salvarInternalStorage() throws IOException {
		String NOME_ARQUIVO = "hello_file";
		String string = "hello world!";

		FileOutputStream fos = openFileOutput(NOME_ARQUIVO,
				Context.MODE_PRIVATE);
		fos.write(string.getBytes());
		fos.close();

		String mSelectionClause = UserDictionary.Words.APP_ID + " LIKE ?";
		String[] mSelectionArgs = { "user" };
		int mRowsDeleted = 0;
		mRowsDeleted = getContentResolver().delete(
				UserDictionary.Words.CONTENT_URI, mSelectionClause,
				mSelectionArgs);

	}

	public void myClickHandler(View view) {

		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {

		} else {

		}

	}
	boolean manterServidor;
	public void iniciaSerividor(int port) throws IOException {
		ServerSocket serv = new ServerSocket(port);
		while (manterServidor) {
			Socket conexao = serv.accept();
			tratarNovaConexao(conexao);
		}

	}
	public void connectarSocket(String ip, int port) throws UnknownHostException, IOException{
		Socket conexao = new Socket(ip, port);
		OutputStream out = conexao.getOutputStream();
		out.write("Olá".getBytes());
	}
	private void tratarNovaConexao(Socket conexao) {
		// TODO Auto-generated method stub
		
	}

	public String InputStreamToString(InputStream in) throws IOException {
		int lido = 0;
		String texto = "";
		byte[] buffer = new byte[1024];
		while ((lido = in.read(buffer)) > 0) {
			texto += new String(buffer, 0, lido);
		}
		in.close();
		return texto;
	}

	public void lerInternalStorage() throws IOException {
		String NOME_ARQUIVO = "hello_file";
		InputStream in = openFileInput(NOME_ARQUIVO);
		int lido = 0;
		String texto = "";
		byte[] buffer = new byte[1024];
		while ((lido = in.read(buffer)) > 0) {
			texto += new String(buffer, 0, lido);
		}
		in.close();
		Log.d("TextoLido", texto);

	}

	public void salvandoExternalStorage() {
		getExternalFilesDir(Environment.DIRECTORY_MUSIC);
	}
}
