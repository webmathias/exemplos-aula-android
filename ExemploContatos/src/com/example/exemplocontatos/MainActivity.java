package com.example.exemplocontatos;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void botao1(View v) {
		ContentResolver cr = getContentResolver();
		Cursor contatos = cr.query(ContactsContract.Contacts.CONTENT_URI, 
				null,// projection,
				null,// selection,
				null, null);

		if (contatos.moveToFirst()){
			do {
				String name = contatos
						.getString(contatos
								.getColumnIndex(ContactsContract
										.Contacts.DISPLAY_NAME));
				String id = contatos
						.getString(contatos
								.getColumnIndex(ContactsContract
										.Contacts._ID));
				Cursor fones = cr.query(Phone.CONTENT_URI, 
						null, 
						Phone.CONTACT_ID+" = ?", 
						new String[]{id}, 
						null);
				String contato= name;
				if(fones.moveToFirst()){
					do{
						String numero = fones.getString(
								fones.getColumnIndex(Phone.NUMBER));
						contato += "\n"+numero;
					}while(fones.moveToNext());
				}
				Toast.makeText(getApplicationContext(),
						"Contato Encontrado:\n" + contato, Toast.LENGTH_SHORT)
						.show();
			} while (contatos.moveToNext());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
