package com.example.exemplointent;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class Tela2 extends Activity {
	String recebido ;
	@Override
	protected void onResume() {
		recebido = getIntent().getStringExtra("qualquer");
		Toast.makeText(getApplicationContext(), recebido, Toast.LENGTH_LONG).show();
		super.onResume();
	}
	public void encerra(View v){
		Intent retorno = new Intent();
		retorno.putExtra("retorno", "Obrigado por dizer:"+recebido);
		setResult(RESULT_OK, retorno);
		finish();
	}
	public void inciatela1(View v) {
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tela2, menu);
		return true;
	}

}
