package com.example.exemplointent;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
	public void iniciaTela2(View v) {
		Intent intent = new Intent(getApplicationContext(), Tela2.class);
		intent.putExtra("qualquer", "ola!!!!");
		startActivityForResult(intent, 49);
	}

	@Override
	protected void onActivityResult(int requestCode, 
			int resultCode, Intent data) {
		if(resultCode == RESULT_OK && requestCode == 49){
			String retorno = data.getStringExtra("retorno");
			Toast.makeText(getApplicationContext(), retorno, Toast.LENGTH_LONG).show();
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
