import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
	public class Tratador implements Runnable {
		private Socket conn;

		public Tratador(Socket conn) {
			super();
			this.conn = conn;
		}

		@Override
		public void run() {
			InputStream in = null;
			OutputStream out = null;
			try {
				in = conn.getInputStream();
				out = conn.getOutputStream();
				byte buff[] = new byte[1024];
				int lido;
				while ((lido = in.read(buff))!= -1 && conn.isConnected() && !conn.isClosed()) {
					 
					if (lido > 0) {
						String texto = new String(buff, 0, lido, "UTF-8");
						System.out.println("Recebido("
								+ conn.getInetAddress().getHostAddress() + "):"
								+ texto);
						out.write(("Resposta do Servidor: " + texto)
								.getBytes("UTF-8"));
					}
				}
			} catch (Throwable e) {

			} finally {
				System.out.println("Desconectado:"
						+ conn.getInetAddress().getHostAddress());
				try {
					conn.close();
					in.close();

				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			 
		}
	}

	public Servidor() throws IOException {
		ServerSocket serv = new ServerSocket(8000);
		System.out.println("Servidor Ligado:");
		
		System.out.println("192.168.0.106:"+serv.getLocalPort());
		while (true) {
			Socket connecxao = serv.accept();
			System.out.println("Conectado:"
					+ connecxao.getInetAddress().getHostAddress());

			new Thread(new Tratador(connecxao)).start();

		}
	}

	public static void main(String[] args) throws IOException {
		new Servidor();

	}
}
