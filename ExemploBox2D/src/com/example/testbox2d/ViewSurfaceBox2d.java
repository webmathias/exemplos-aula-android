package com.example.testbox2d;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.jbox2d.collision.AABB;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.ShapeType;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.joints.DistanceJointDef;
import org.jbox2d.dynamics.joints.Joint;
import org.jbox2d.dynamics.joints.JointType;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.JsonReader;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class ViewSurfaceBox2d extends SurfaceView implements
		SurfaceHolder.Callback {
	public ViewSurfaceBox2d(Context context) {
		super(context);
		getHolder().addCallback(this);
		
	}

	@Override
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	World world;
	 
	 

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		Vec2 gravity = new Vec2((float) 0, (float) 10f);
		boolean doSleep = true;

		world = new World(gravity, doSleep);

		PolygonShape polygonShape1 = new PolygonShape();
		polygonShape1.setAsBox(100, 10);

		BodyDef bodyDef1 = new BodyDef();
		bodyDef1.type = BodyType.STATIC;
		bodyDef1.position.set(100 / 2, 150);
		// bodyDef.angle = (float) (Math.PI / 4 * j);
		bodyDef1.allowSleep = false;
		Body chao = world.createBody(bodyDef1);
		chao.createFixture(polygonShape1, 5.0f).setFriction(100);
 
		
		//circulo
		CircleShape polygonShape = new CircleShape();
		 polygonShape.m_radius =  0.5f;

		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		 
		bodyDef.fixedRotation = true;
		bodyDef.position.set(50, 50);

		bodyDef.allowSleep = true;

		Body b = world.createBody(bodyDef);
		b.createFixture(polygonShape,  10).setFriction(1);
 
		new Thread() {
			public void run() {
				vivo = true;
				Paint p = new Paint();
				long time = System.currentTimeMillis();
				float timeStep = 2f / 60.0f;
				int velocityIterations = 12;
				int positionIterations = 20;
				float fps = 0;
				while (vivo) {
					Canvas canvas = getHolder().lockCanvas();
					if (canvas != null) {

						// canvas.scale(5f, 5f);
						p.setColor(Color.WHITE);
						canvas.drawRect(0, 0, getWidth(), getHeight(), p);
						 
						world.step(timeStep, velocityIterations,
								positionIterations);
						if (mexeu) {
							mexeu();
							mexeu = false;
						}
						p.setColor(Color.BLUE);
						for(Body b = world.getBodyList();b!=null;b = b.m_next){
							canvas.drawCircle(b.getPosition().x, b.getPosition().y, 5, p);
							canvas.drawText("y:"+50, 15, b.getPosition().y, p);
							 
						}
						time = System.currentTimeMillis() - time;
						fps = 1000f / (float) time;

						time = System.currentTimeMillis();
						getHolder().unlockCanvasAndPost(canvas);
					}

				}

			};
		}.start();

	}

	float fator = 4f;
	boolean vivo;
	float x, y;
	boolean mexeu = false;

	public void mexeu() {
		 
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		x = event.getX() / fator;
		y = event.getY() / fator;
		mexeu = true;

		return true;
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

	}
}
