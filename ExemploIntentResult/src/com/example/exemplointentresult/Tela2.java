package com.example.exemplointentresult;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Tela2 extends Activity {
	public void voltar(View v){
		EditText ed = (EditText)findViewById(R.id.editText1);
		String texto = ed.getText().toString();
		
		Intent in = new Intent();
		in.putExtra("retorno", texto);
		setResult(RESULT_OK, in);
		finish();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela2);
		String param = getIntent().getStringExtra("param");
		TextView txt = (TextView)findViewById(R.id.textView3);
		txt.setText(param);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tela2, menu);
		return true;
	}

}
