package com.example.exemplointentresult;

import java.io.File;
import java.io.IOException;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	File file;

	public void foto(View v) {
		file = new File(Environment.getExternalStorageDirectory(), "foto.jpg");

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, 
				Uri.fromFile(file));
		intent.putExtra("return-data", false);

		startActivityForResult(intent, 98);
	}

	public void enviar(View v) {
		Log.d("Tag Teste", "Passei pelo enviar");
		Toast.makeText(getApplicationContext(), "Passei por aqui no Toast",
				Toast.LENGTH_LONG).show();

		EditText ed = (EditText) findViewById(R.id.editText1);
		String texto = ed.getText().toString();
		Intent in = new Intent(getApplicationContext(), Tela2.class);
		in.putExtra("param", texto);
		startActivityForResult(in, 99);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 99 && resultCode == RESULT_OK) {
			String retorno = data.getStringExtra("retorno");
			TextView txt = (TextView) findViewById(R.id.textView3);
			txt.setText(retorno);
		}
		if (requestCode == 98 && resultCode == RESULT_OK) {
			ImageView imageview = (ImageView)
					findViewById(R.id.imageView1);
			Bitmap btp = null;
			try {
				btp = BitmapFactory.decodeFile(file.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			imageview.setImageBitmap(btp);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
