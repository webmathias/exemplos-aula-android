package com.example.exemplogps;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	TextView txt;
	TextView txt1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		txt = (TextView) findViewById(R.id.textview1);
		txt1 = (TextView) findViewById(R.id.textView2);
		LocationManager locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location l) {
				txt.setText("" + l.getLatitude() + ", " + l.getLongitude()
						+ ", " + l.getAltitude() + ", " + l.getAccuracy());
				// location.getLatitude()
				// location.getLongitude();
				// location.getAltitude();
				// location.getAccuracy();
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				txt.setText(txt.getText() + "\nstatus Changed:" + status);

			}

			public void onProviderEnabled(String provider) {
				txt.setText(txt.getText() + "\n enabled");

			}

			public void onProviderDisabled(String provider) {
				txt.setText(txt.getText() + "\n disabled");
			}
		};

		// locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
		// 0, 0, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
				0, locationListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
