package com.example.exemplostyles;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		Dialog dialog = new Dialog(MainActivity.this, android.R.style.Theme_Translucent);
		dialog.setCancelable(true);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog);
		SVG svg;
		try {
			svg = SVG.getFromResource(getApplicationContext(), R.raw.bg);

			// Picture picture = svg.getPicture();
			if (svg.getDocumentWidth() != -1) {
				Bitmap newBM = Bitmap.createBitmap((int) Math.ceil(svg.getDocumentWidth()), (int) Math.ceil(svg.getDocumentHeight()), Bitmap.Config.ARGB_8888);
				Canvas bmcanvas = new Canvas(newBM);
				// Clear background to white
				// bmcanvas.drawRGB(255, 255, 255);
				// Render our document onto our canvas
				svg.renderToCanvas(bmcanvas);
				
				// ((ImageView)
				// dialog.findViewById(R.id.imageView1)).setImageBitmap(newBM);
				RelativeLayout l = (RelativeLayout) dialog.findViewById(R.id.dialogo);

				l.setBackground(new BitmapDrawable(getResources(), newBM));
				// l.setRotation(10);
				// l.setAlpha(0);
				l.setAlpha(0f);
				l.setVisibility(View.VISIBLE);
				l.setScaleX(0);
				l.setScaleY(0);
				l.animate().alpha(1).setDuration(600).scaleX(1).scaleY(1).start();
				dialog.setOnCancelListener(new  OnCancelListener() {
					
					@Override
					public void onCancel(DialogInterface dialog) {
						((Dialog)dialog).findViewById(R.id.dialogo).animate().alpha(0.1f).setDuration(600).scaleX(0.1f).scaleY(0.1f).start();
					}
				});
			}

		} catch (SVGParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dialog.show();
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
