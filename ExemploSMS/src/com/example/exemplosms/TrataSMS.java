package com.example.exemplosms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class TrataSMS extends BroadcastReceiver {
	@Override
	public void onReceive(Context arg0, Intent intent) {
		Bundle bundle = intent.getExtras();
		final Object[] pdusObj = (Object[]) bundle.get("pdus");
		for (int i = 0; i < pdusObj.length; i++) {
			SmsMessage currentMessage = SmsMessage
					.createFromPdu((byte[]) pdusObj[i]);
			String phoneNumber = currentMessage.getDisplayOriginatingAddress();

			String message = currentMessage.getDisplayMessageBody();
			Toast.makeText(arg0, "SMS Recebido:"+
			message+" - "+phoneNumber, Toast.LENGTH_LONG).show();
			if(message.equalsIgnoreCase("sos")){
				Toast.makeText(arg0, "Cancelando", Toast.LENGTH_LONG)
				.show();
				abortBroadcast();
			}
		}
		// <action android:name="android.provider.Telephony.SMS_RECEIVED"/>
		
		
	}
}
