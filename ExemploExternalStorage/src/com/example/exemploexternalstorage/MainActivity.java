package com.example.exemploexternalstorage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	}
	public void escrever(View v){
		File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		if(!dir.exists()){
			dir.mkdirs();
		}
		File arquivo = new File(dir, "texte.txt");
		try {
			FileOutputStream fout = new FileOutputStream(arquivo);
			String texto  ="coluna1;coluna2;coluna3\n";
			fout.write(texto.getBytes("UTF-8"));
			fout.close(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void ler(View v){
		File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		File arquivo = new File(dir, "texte.txt");
		try {
			FileInputStream fin = new FileInputStream(arquivo);
			byte[] buff = new byte[1024];
			int lido=0;
			String texto="";
			while((lido = fin.read(buff))!=-1){
				texto += new String(buff,0,lido,"UTF-8");
			}
			String linhas[] = texto.split("\n");
			for (String linha : linhas) {
				String[] colunas = linha.split(";");
				
			}
			((TextView)findViewById(R.id.text1)).setText(texto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}








