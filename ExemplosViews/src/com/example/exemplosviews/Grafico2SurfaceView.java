package com.example.exemplosviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class Grafico2SurfaceView extends SurfaceView implements SurfaceHolder.Callback {

	public Grafico2SurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		getHolder().addCallback(this);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub

	}

	boolean vivo;
	Thread t;
	int x = 0;
	long diftime;
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		t = new Thread() {
			public void run() {
				vivo = true;
				Paint p = new Paint();
				SurfaceHolder h = getHolder();
				while (vivo) {
					
					long time = System.currentTimeMillis();
					Canvas c = h.lockCanvas();
					if(c != null){
//						c.drawArc(oval, startAngle, sweepAngle, useCenter, paint)
						c.drawColor(Color.WHITE);
						p.setColor(Color.MAGENTA);
						c.drawLine(x, 0, getWidth(), getHeight(), p);
						x++;
						p.setColor(Color.BLACK);
						c.drawText("Diftime:"+diftime, 50, 50, p);
						h.unlockCanvasAndPost(c);
					}
					long time1 = System.currentTimeMillis();
					diftime = time1-time;

				}

			};
		};
		t.start();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		vivo = false;
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
