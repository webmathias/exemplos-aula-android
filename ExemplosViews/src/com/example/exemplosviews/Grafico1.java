package com.example.exemplosviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class Grafico1 extends View{
	Thread t ;
	public Grafico1(Context context, AttributeSet attrs) {
		super(context, attrs);
		 
	}
	float w = 0;
	float h = 0;
	
	Object controle = new Object();
	 @Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		 if(t == null){
				t = new Thread(){
					public void run() {
						for (int i = 0; i < 500; i++) {
							w++;
							h++;			

							try {
								synchronized (controle) {
									controle.wait(50);	
								}							
							} catch (Exception e) {
							}
							postInvalidate();
						}
						
					};
				};
				t.start();
			}
		return super.onTouchEvent(event);
	}
	@Override
	protected void onDraw(Canvas canvas) {
		
		Paint p = new Paint();
		p.setColor(Color.MAGENTA);
		float x1 = 0;
		float y1 = (getHeight()/2f)-h/2;
		float x2 = (getWidth()/2f)+w/2;
		float y2 = (getHeight()/2f)+h/2;
		 canvas.drawRect(x1,y1,x2,y2,p);
		super.onDraw(canvas);
	}

}
