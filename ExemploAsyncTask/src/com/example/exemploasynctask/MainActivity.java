package com.example.exemploasynctask;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private class MinahTarefa extends AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... arg0) {
			for (int i = 0; i < 10; i++) {

				try {
					Thread.sleep(5000);
					this.publishProgress(i);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			Toast.makeText(getApplicationContext(), "Antes de Começar",
					Toast.LENGTH_SHORT).show();
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			Toast.makeText(getApplicationContext(), "Progresso: " + values[0],
					Toast.LENGTH_SHORT).show();
			((TextView) findViewById(R.id.textview1)).setText("Progresso: "
					+ values[0]);
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			Toast.makeText(getApplicationContext(), "Depois de Terminar",
					Toast.LENGTH_SHORT).show();
			super.onPostExecute(result);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		new MinahTarefa().execute("teste", "teste1");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
