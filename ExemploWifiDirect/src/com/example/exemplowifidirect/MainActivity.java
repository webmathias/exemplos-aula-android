package com.example.exemplowifidirect;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
	WifiP2pManager mManager;
	Channel mChannel;
	BroadcastReceiver mReceiver;
	IntentFilter mIntentFilter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
		mChannel = mManager.initialize(this, getMainLooper(), null);
		mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
		mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		registerReceiver(mReceiver, mIntentFilter);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceiver(mReceiver);
		super.onPause();
	}
	public void discobre(View v){
		WifiApManager m = new WifiApManager(this);
		 Toast.makeText(MainActivity.this, "State:"+m.getWifiApState(), Toast.LENGTH_SHORT).show();
//		 WifiConfiguration
		 m.setWifiApEnabled(null, true);
		 Toast.makeText(MainActivity.this, "State:"+m.getWifiApState(), Toast.LENGTH_SHORT).show();
		mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
		    @Override
		    public void onSuccess() {
		    	 Toast.makeText(MainActivity.this, "Socesso ao descobrir pontos", Toast.LENGTH_SHORT).show();
		    }

		    @Override
		    public void onFailure(int reasonCode) {
		    	switch (reasonCode) {
				case WifiP2pManager.BUSY:
					Toast.makeText(MainActivity.this, "Falha ao descobrir pontos BUSY:"+reasonCode, Toast.LENGTH_SHORT).show();
					break;
				case WifiP2pManager.P2P_UNSUPPORTED:
					Toast.makeText(MainActivity.this, "Falha ao descobrir pontos P2P Não suportado:"+reasonCode, Toast.LENGTH_SHORT).show();
					break;
				case WifiP2pManager.ERROR:
					Toast.makeText(MainActivity.this, "Falha ao descobrir pontos ERROR:"+reasonCode, Toast.LENGTH_SHORT).show();
					break;

				default:
					Toast.makeText(MainActivity.this, "Falha ao descobrir pontos:"+reasonCode, Toast.LENGTH_SHORT).show();
					break;
				}
		    	 
		    }
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
